class Rectangle{
    constructor(w,l){
        this.width=w;
        this.length=l;
    }
    area(){
        return (this.width*this.length);
    }
}
const area1=new Rectangle(4,5);
const area2=new Rectangle(5,8);
console.log('Area=',area1.area());
console.log('Area=',area2.area());
function print(){
    document.getElementById("para1").innerHTML=`Area of a Rectangle passing value(4,5):${area1.area()}`
    document.getElementById("para2").innerHTML=`Area of a Rectangle passing value(5,8):${area2.area()}`
}